using Microsoft.Office.Interop.Excel;
using QueryStorm.Data;
using QueryStorm.Apps;
using System;
using Unity;
using Unity.Lifetime;
using static QueryStorm.Tools.DebugHelpers;
using Windy.Query.Services;
using QueryStorm.Apps.Contract;

namespace Windy.Query
{
	public class App : AppBase
	{
		public App(IAppHost host)
			:base(host)
		{
			// Register services here to make them available to components via DI, for example:
            // container.RegisterInstance(new MyServiceAbc())
            Container.RegisterType<WorkbookDataContextGetter>(new ContainerControlledLifetimeManager());
		}

        public override void Start()
        {
            base.Start();
    		TempLicensing.ActivateTestLicenseIfNeeded(Host.DataStore, "4d807b14-4bc6-4f6b-b2a3-6e9cd28ffcbe", "a0d0617b-e8c8-472c-bca8-79ec92b427ef");
        }
    }
}