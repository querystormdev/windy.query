using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SQLite;
using System.Drawing;
using System.Linq;
using System.Text;
using Microsoft.Office.Interop.Excel;
using QueryStorm.Apps.Contract;
using QueryStorm.Data;
using QueryStorm.Engines;
using QueryStorm.Tools.Excel;
using static QueryStorm.Tools.DebugHelpers;

namespace Windy.Query;

[Category("Excel")]
public class WorkbookSQLiteFunctions
{
    readonly IWorkbookAccessor workbookAccessor;
    public WorkbookSQLiteFunctions(IWorkbookAccessor workbookAccessor)
    {
        this.workbookAccessor = workbookAccessor;
    }

    /// <summary>
    /// Sets the background color of the range.
    /// </summary>
    /// <param name="address">The address of the target range.</param>
    /// <param name="colorName">The name of the color to set.</param>
    [SQLFunc]
    public string SetBackgroundColor(string address, string colorName)
    {
        Color color = colorName.StartsWith("#") ? ColorTranslator.FromHtml(colorName) : Color.FromName(colorName);
        var xlColor = ExcelHelpers.Color(color);
        workbookAccessor.Run(w => w.ProcessRange(address, r => r.Interior.Color = xlColor));
        return address;
    }

    /// <summary>
    /// Sets the background color of the range.
    /// </summary>
    /// <param name="address">The address of the target range.</param>
    /// <param name="red">The red component.</param>
    /// <param name="green">The green component.</param>
    /// <param name="blue">The blue component.</param>
    [SQLFunc]
    public string SetBackgroundColor(string address, int red, int green, int blue)
    {
        var xlColor = ExcelHelpers.Color(red, green, blue);
        workbookAccessor.Run(w => w.ProcessRange(address, r => r.Interior.Color = xlColor));
        return address;
    }

    /// <summary>
    /// Sets the background color of the range.
    /// </summary>
    /// <param name="address">The address of the target range.</param>
    ///<example>
    ///<![CDATA[SELECT
    /// CASE JobTitle
    ///     WHEN 'Chief Executive Officer' THEN SetBackgroundColor(__address, 'red')
    ///     WHEN 'Vice President of Engineering' THEN SetBackgroundColor(__address, 'yellow')
    ///     WHEN 'Research and Development Manager' THEN SetBackgroundColor(__address, 'teal')
    /// ELSE 
    ///     ClearBackgroundColor(__address)
    /// END
    ///FROM
    /// Employee]]>
    ///</example>
    [SQLFunc]
    public string ClearBackgroundColor(string address)
    {
        workbookAccessor.Run(w => w.ProcessRange(address, r => r.Interior.ColorIndex = 0));
        return address;
    }

    /// <summary>
    /// Sets the font color of the range.
    /// </summary>
    /// <param name="address">The address of the target range.</param>
    /// <param name="colorName">The name of the color to set.</param>
    [SQLFunc]
    public string SetFontColor(string address, string colorName)
    {
        Color color = colorName.StartsWith("#") ? ColorTranslator.FromHtml(colorName) : Color.FromName(colorName);
        var xlColor = ExcelHelpers.Color(color);
        workbookAccessor.Run(w => w.ProcessRange(address, r => r.Font.Color = xlColor));
        return address;
    }

    /// <summary>
    /// Sets the font color of the range.
    /// </summary>
    /// <param name="address">The address of the target range.</param>
    /// <param name="red">The red component.</param>
    /// <param name="green">The green component.</param>
    /// <param name="blue">The blue component.</param>
    [SQLFunc]
    public string SetFontColor(string address, int red, int green, int blue)
    {
        var xlColor = ExcelHelpers.Color(red, green, blue);
        workbookAccessor.Run(w => w.ProcessRange(address, r => r.Font.Color = xlColor));
        return address;
    }

    /// <summary>
    /// Gets the value of the specified cell.
    /// </summary>
    /// <param name="address"></param>
    /// <returns></returns>
    [SQLFunc]
    public object GetCellValue(string address)
    {
        var value = workbookAccessor.Eval(w => w.GetRange(address).Value);
        if (value is DateTime dt)
            return dt.ToString("s");
        else
            return value;
    }

    /// <summary>
    /// Gets the value of the specified cell.
    /// </summary>
    /// <param name="address"></param>
    /// /// <param name="value"></param>
    /// <returns></returns>
    [SQLFunc]
    public void SetCellValue(string address, object value)
    {
        workbookAccessor.Run(w => w.Application.WhenReady(() => w.GetRange(address).Value = value));
    }

    /// <summary>
    /// Gets the formula in the specified cell.
    /// </summary>
    /// <param name="address"></param>
    /// <returns></returns>
    [SQLFunc]
    public object GetCellFormula(string address)
    {
        return workbookAccessor.Eval(w => w.GetRange(address).Formula);
    }

    /// <summary>
    /// Sets the formula in the specified cell.
    /// </summary>
    /// <param name="address"></param>
    /// <param name="formula"></param>
    /// <returns></returns>
    [SQLFunc]
    public void SetCellFormula(string address, string formula)
    {
        workbookAccessor.Run(w => w.Application.WhenReady(() => w.GetRange(address).Formula = formula));
    }

    /// <summary>
    /// Refresh the pivot table with the specified name
    /// </summary>
    /// <param name="pivotTableName"></param>
    /// <returns></returns>
    [SQLFunc]
    public void RefreshPivotTable(string pivotTableName)
    {
        bool found = false;
        workbookAccessor.Run(w =>
        {
            foreach (Worksheet worksheet in w.Worksheets)
            {
                foreach (PivotTable pivotTable in (dynamic)worksheet.PivotTables())
                {
                    if (pivotTable.Name == pivotTableName)
                    {
                        pivotTable.RefreshTable();
                        found = true;
                    }
                }
            }
            if (!found)
                throw new ArgumentException($"Pivot table '{pivotTableName}' not found!");
        });
    }

    [SQLFunc]
    public object Vba(object[] args)
    {
        return workbookAccessor.Eval(w => w.Application.Run((string)args[0],
                    args.Length > 1 ? args[1] : Type.Missing,
                    args.Length > 2 ? args[2] : Type.Missing,
                    args.Length > 3 ? args[3] : Type.Missing,
                    args.Length > 4 ? args[4] : Type.Missing,
                    args.Length > 5 ? args[5] : Type.Missing,
                    args.Length > 6 ? args[6] : Type.Missing,
                    args.Length > 7 ? args[7] : Type.Missing,
                    args.Length > 8 ? args[8] : Type.Missing,
                    args.Length > 9 ? args[9] : Type.Missing,
                    args.Length > 10 ? args[10] : Type.Missing,
                    args.Length > 11 ? args[11] : Type.Missing,
                    args.Length > 12 ? args[12] : Type.Missing,
                    args.Length > 13 ? args[13] : Type.Missing,
                    args.Length > 14 ? args[14] : Type.Missing,
                    args.Length > 15 ? args[15] : Type.Missing,
                    args.Length > 16 ? args[16] : Type.Missing,
                    args.Length > 17 ? args[17] : Type.Missing,
                    args.Length > 18 ? args[18] : Type.Missing,
                    args.Length > 19 ? args[19] : Type.Missing,
                    args.Length > 20 ? args[20] : Type.Missing,
                    args.Length > 21 ? args[21] : Type.Missing,
                    args.Length > 22 ? args[22] : Type.Missing,
                    args.Length > 23 ? args[23] : Type.Missing,
                    args.Length > 24 ? args[24] : Type.Missing,
                    args.Length > 25 ? args[25] : Type.Missing,
                    args.Length > 26 ? args[26] : Type.Missing,
                    args.Length > 27 ? args[27] : Type.Missing,
                    args.Length > 28 ? args[28] : Type.Missing,
                    args.Length > 29 ? args[29] : Type.Missing,
                    args.Length > 30 ? args[30] : Type.Missing));
    }

    /// <summary>
    /// Returns a list of cells in the specified range. If range is not specified, the cells in the current selection (intersected with the used range) are returned. Supports SQL UPDATE.
    /// </summary>
    /// <param name="targetRangeAddress">The target range</param>
    /// <returns></returns>
    [SQLFuncTabular]
    public IEnumerable<CellAccessor> XLCells(string targetRangeAddress = null)
    {
        return workbookAccessor.Eval(w => new CellsAccessor(w.GetUsedSelectionRange(targetRangeAddress)));
    }
}

