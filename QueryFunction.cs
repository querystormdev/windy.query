using System;
using Microsoft.Office.Interop.Excel;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using QueryStorm.Tools;
using static QueryStorm.Tools.DebugHelpers;
using Antlr4.Runtime;
using QueryStorm.Data;
using QueryStorm.Engines;
using QueryStorm.Apps;
using QueryStorm.Engines.Sqlite;
using Windy.Query.Services;
using QueryStorm.Apps.Contract;
using System.Diagnostics;
using QueryStorm.Engines.SQLite.Antlr;
using QueryStorm.Tools.Excel;

namespace Windy.Query
{
    public class QueryFunction
    {
        WorkbookDataContextGetter contextGetter;
        private readonly ILicensingService licensingService;
        private readonly IQueryStormLogger logger;
        private readonly IExcelAccessor excelAccessor;

        public QueryFunction(WorkbookDataContextGetter contextGetter, ILicensingService licensingService, IQueryStormLogger logger, IExcelAccessor excelAccessor)
        {
            this.contextGetter = contextGetter;
            this.licensingService = licensingService;
            this.logger = logger;
            this.excelAccessor = excelAccessor;
        }

        [ExcelFunction(Name = "Windy.Query", Description = "Runs a SQL query that has access to workbook tables and returns the results.")]
        public IValueStream Query(
        	CallerInfo callerInfo,
            string queryText,
            [ExcelArgument(Description = "If true, headers will be included as the first row of the results.")]
            bool returnHeaders = false,
            [ExcelArgument(Description = "If true, results will update whenever referenced tables and variables change.")]
            bool autoRefresh = false)
        {
        	if(!autoRefresh)
        		licensingService.EnsureAccess(1, "Query");
    		else
    			licensingService.EnsureAccess(2, "QueryWithAutoRefresh");
    			
            return new QueryResultsStream(callerInfo, excelAccessor, contextGetter, queryText, returnHeaders, autoRefresh, logger);
        }

        class QueryResultsStream : IValueStream
        {
        	CancellationTokenSource cts = new CancellationTokenSource();
            WorkbookDataContext context;
            IDataContext filteredContext;
            private readonly CallerInfo callerInfo;
            private readonly IExcelAccessor excelAccessor;
            private readonly WorkbookDataContextGetter contextGetter;
            string query;
            bool returnHeaders, autoRefresh;
            private readonly IQueryStormLogger logger;

            public QueryResultsStream(CallerInfo callerInfo, IExcelAccessor excelAccessor, WorkbookDataContextGetter contextGetter, string query, bool returnHeaders, bool autoRefresh, IQueryStormLogger logger)
            {
                this.callerInfo = callerInfo;
                this.excelAccessor = excelAccessor;
                this.contextGetter = contextGetter;
                this.query = query;
                this.returnHeaders = returnHeaders;
                this.autoRefresh = autoRefresh;
                this.logger = logger;
            }

            public void Stop()
            {
                if (context != null)
                {
                	context.SchemaChanged -= OnSchemaChanged;
                }
                cts.Cancel();
            }

            ContextBoundEngine ctxEng;

            public event System.Action Completed;
            public event Action<Exception> Error;
            public event Action<object> Next;

        	Guid guid = Guid.NewGuid();

            public async void Start()
            {
                try
                {
                	logger.Trace($"Starting query {guid}: {query}");
                	
                	var listener = new SQLiteUsedInputsListener();
                    var lexer = new SQLiteLexer(new AntlrInputStream(query));
                    var parser = new SQLiteParser(new CommonTokenStream(lexer));
                    parser.AddParseListener(listener);
                    parser.parse();

                    var usedTables = new HashSet<string>(listener.UsedTableNames, StringComparer.OrdinalIgnoreCase);
                    var usedVariables = new HashSet<string>(listener.UsedVariableNames, StringComparer.OrdinalIgnoreCase);

                    // todo: add an error listener and display error messages in the log

                    var sqliteEngine = new SQLiteEngine();
                    await sqliteEngine.ConnectAsync(cts.Token);
                    
                    // add workbook interaction functions
                    await sqliteEngine.BindFunctionsAsync(new WorkbookSQLiteFunctions(new ByNameWorkbookAccessor(excelAccessor, callerInfo.WorkbookName)), cts.Token);

					context = await contextGetter.GetAsync(callerInfo, cts.Token);
                    filteredContext = new FilteredDataContext(context, usedTables.Contains, usedVariables.Contains);
                    
                    ctxEng = new ContextBoundEngine(sqliteEngine, filteredContext);

                    await ctxEng.BindAsync();

                    await RunQuery(ctxEng);
            		
                    if (autoRefresh)
                    	context.SchemaChanged += OnSchemaChanged;
                }
                catch (Exception ex)
                {
                    Error?.Invoke(ex);
                    logger.Error($"An error occured while starting the function {guid}", new Exception(ex.Message));
                }
            }

            private async void OnSchemaChanged(object sender, EventArgs args)
            {
                try
                {
                    await RunQuery(ctxEng);
                }
                catch (Exception ex)
                {
                    Error?.Invoke(ex);
                    logger.Error($"An error occured while re-running the function {guid}", new Exception(ex.Message));
                }
            }

            private async Task RunQuery(ContextBoundEngine engine)
            {            
            	logger.Trace($"Running query {guid}: {query}");
                var result = await engine.ExecuteAsync(query, cts.Token);
                
                logger.Trace($"Saving changes after query {guid}");
                await context.SaveChangesAsync(cts.Token);
                
                logger.Trace($"Outputting results {guid}");
                Next?.Invoke(result == null ? null : result.ToExcelValues2DArray(returnHeaders));
                if (!autoRefresh)
                {
                	logger.Trace($"Query {guid} completed.");
                    Completed?.Invoke();
                }
            }
        }
    }
}