using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Office.Interop.Excel;
using QueryStorm.Apps;
using QueryStorm.Apps.Contract;
using QueryStorm.Data;
using QueryStorm.Tools;
using QueryStorm.Tools.Excel;
using static QueryStorm.Tools.DebugHelpers;

namespace Windy.Query.Services
{
    public class WorkbookDataContextGetter : IDisposable
    {
        private readonly IExcelAccessor excelAccessor;
        Dictionary<Workbook, WorkbookDataContext> cache = new Dictionary<Workbook, WorkbookDataContext>();

        ISyncRunner syncRunner;
        public WorkbookDataContextGetter(IExcelAccessor excelAccessor, ISyncRunner syncRunner)
        {
            this.excelAccessor = excelAccessor;
            this.syncRunner = syncRunner;
        }

        class NoColTypeAutoDetectWorkbookDataContext : WorkbookDataContext
        {
            public NoColTypeAutoDetectWorkbookDataContext(IWorkbookAccessor acc, ISyncRunner syncRunner, IWorkbookDataEventsSource e)
                : base(acc, syncRunner, e)
            {
            }
        }

        public async Task<WorkbookDataContext> GetAsync(CallerInfo callerInfo, CancellationToken cancellationToken)
        {
            var res = excelAccessor.Eval(app =>
            {
                var workbooks = app.Workbooks.OfType<Workbook>().Select(w => w.FullName).ToList();
                var workbook = app.GetWorkbook(callerInfo.WorkbookName);
                if (cache.TryGetValue(workbook, out var service))
                    return new { service, isNew = false };
                else
                {
                	var workbookAccessor = new SimpleWorkbookAccessor(workbook, syncRunner);
                	var eventsSource = new DebouncedWorkbookEventsSource(
                		workbookAccessor, 
                		new BasicWorkbookChangedDataEventsSource(workbookAccessor));
                
                    var context = new NoColTypeAutoDetectWorkbookDataContext(workbookAccessor, syncRunner, eventsSource);
                    
                    workbook.AddClosedHandler(() => cache.Remove(workbook));
                    
                    return new { service = cache[workbook] = context, isNew = true };
                }
            });
            
            if(res.isNew)
            	await res.service.EnsureInitializedAsync(cancellationToken);
            	
    		return res.service;
        }

        public void Dispose()
        {
            cache.Values.ForEach(x => x.Dispose());
        }
    }
}
