using System;
using System.Collections.Generic;
using System.Linq;
using System.Management;
using System.Net.Http;
using System.Net.NetworkInformation;
using System.Text;
using QueryStorm.Apps.Contract;
using static QueryStorm.Tools.DebugHelpers;

namespace Windy.Query;

public class TempLicensing
{
    public static void ActivateTestLicenseIfNeeded(IDataStore dataStore, string productId, string keyId)
    {
        try
        {
        	string licFilePath = @$"License\{productId}.lic";
            if (!dataStore.FileExists(licFilePath))
            {
                var formContent = new FormUrlEncodedContent(new[]
				{
				    new KeyValuePair<string, string>("ProductId", productId), 
				    new KeyValuePair<string, string>("Hwid", LicenceContextLoader.GetHwId()),
				    new KeyValuePair<string, string>("Username", LicenceContextLoader.GetUserName()), 
				    new KeyValuePair<string, string>("FeatureSet", "1"),
				    new KeyValuePair<string, string>("KeyId", keyId) 
				});
                
                var resp = new HttpClient().PostAsync("https://keystodian.com/api/licensing", formContent).Result;
                var json = resp.Content.ReadAsStringAsync().Result;
                if(!json.Contains("Key not valid for this product"))
                	dataStore.Save(licFilePath, json);
            }
        }
        catch
        {
        
        }
    }

    public class LicenceContextLoader
    {
        public static string GetUserName() => $@"{Environment.UserDomainName}\{Environment.UserName}";

        public static string GetHwId()
        {
            string hardwareId = string.Empty;

            try
            {
                using (var searcher = new ManagementObjectSearcher("SELECT ProcessorId, Name FROM Win32_Processor"))
                using (var collection = searcher.Get())
                {
                    foreach (ManagementObject oManagementObject in collection)
                    {
                        hardwareId = (string)oManagementObject["ProcessorId"];
                        break;
                    }
                }
            }
            catch { }

            bool hddMatched = false;
            try
            {
                using (var searcher = new ManagementObjectSearcher("SELECT * FROM Win32_DiskDrive"))
                using (var collection = searcher.Get())
                {
                    foreach (ManagementObject drive in searcher.Get())
                    {
                        string interfaceType = drive.GetPropertyValue("InterfaceType") as string;
                        if (interfaceType != null && interfaceType.ToString().Contains("USB"))
                            continue;

                        string hddid = drive.GetPropertyValue("SerialNumber") as string;
                        if (!string.IsNullOrWhiteSpace(hddid))
                        {
                            hddMatched = true;
                            hardwareId += hddid;
                            break;
                        }
                    }
                }
            }
            catch { }

            if (hddMatched == false)
            {
                try
                {
                    using (var searcher = new ManagementObjectSearcher("SELECT * FROM Win32_NetworkAdapter"))
                    using (var collection = searcher.Get())
                    {
                        foreach (ManagementObject drive in searcher.Get())
                        {
                            string macAddress = drive.GetPropertyValue("MACAddress") as string;
                            hardwareId += macAddress;
                            break;
                        }
                    }
                }
                catch
                {
                    hardwareId += GetMacAddress();
                }
            }

            return hardwareId;
        }

        /// <summary>
        /// Finds the MAC address of the first operation NIC found.
        /// </summary>
        /// <returns>The MAC address.</returns>
        private static string GetMacAddress()
        {
            string macAddresses = string.Empty;

            foreach (var nic in NetworkInterface.GetAllNetworkInterfaces())
            {
                if (nic.OperationalStatus == OperationalStatus.Up)
                {
                    macAddresses += nic.GetPhysicalAddress().ToString();
                    break;
                }
            }

            return macAddresses;
        }
    }
}

